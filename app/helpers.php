<?php

use Carbon\Carbon;

function getHour()
{
    // Create a new Carbon instance with the current time and the default timezone
    $now = Carbon::now();
    // Set the timezone of the Carbon instance to Makassar
    $now->setTimezone('Asia/Makassar');
    // Get the hour in the Makassar timezone
    $hour = $now->format('H:i:s');
    return $hour;
}

function getToday()
{
    // Create a new Carbon instance with the current time and the default timezone
    $now = Carbon::now();
    // Set the timezone of the Carbon instance to Makassar
    $now->setTimezone('Asia/Makassar');
    // Get the hour in the Makassar timezone
    $hour = $now->format('Y-m-d');
    return $hour;
}
