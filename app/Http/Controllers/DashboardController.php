<?php

namespace App\Http\Controllers;

use App\Models\Pesanan;
use Illuminate\Http\Request;
use Inertia\Inertia;

class DashboardController extends Controller
{
    public function index($tgl_awal = null, $tgl_akhir = null)
    {
        if (!$tgl_awal || !$tgl_akhir) {
            $pesanan = Pesanan::with('pembayaran')->with('pelanggan')->with('produk')->get();
        } else {
            $pesanan = Pesanan::with('pembayaran')->with('pelanggan')->with('produk')->whereBetween('created_at', [$tgl_awal, $tgl_akhir])->get();
        }

        return Inertia::render('Dashboard', [
            'user' => auth()->user(),
            'pesanan' => $pesanan,
        ]);
    }
}
