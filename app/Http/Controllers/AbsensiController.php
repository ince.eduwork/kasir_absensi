<?php

namespace App\Http\Controllers;

use App\Models\Absensi;
use App\Models\User;
use Illuminate\Http\Request;
use Inertia\Inertia;

class AbsensiController extends Controller
{
    public function absensi()
    {
        return Inertia::render('Absensi', [
            'user' => auth()->user(),
        ]);
    }
    public function check_in(Request $request)
    {
        $hariIni = getToday();
        $cekAbsen = Absensi::where('tgl_absen', $hariIni)->where('user_id', auth()->user()->id)->first();
        if ($cekAbsen) {
            return response()->json([
                'message' => 'anda sudah melakukan absen masuk pada ' . $cekAbsen->check_in . ' hari ini ( ' . getToday() . ')',
            ], 200);
        }

        $absensi = Absensi::create([
            'user_id' => auth()->user()->id,
            'tgl_absen' => getToday(),
            'check_in' => getHour(),
            'check_out' => null,
            'check_in_long_lat' => $request->lat . ' , ' . $request->long,
            'check_out_long_lat' => null,
        ]);

        return response()->json([
            'message' => 'absen masuk berhasil terekam untuk ' . $hariIni,
            'absensi' => $absensi,
        ], 200);
    }

    public function check_out(Request $request)
    {
        $hariIni = getToday();
        $cekAbsen = Absensi::where('tgl_absen', $hariIni)->where('user_id', auth()->user()->id);
        if ($cekAbsen->first()) {
            if ($cekAbsen->first()->check_out) {
                return response()->json([
                    'message' => 'anda sudah melakukan absen pulang pada ' . $cekAbsen->first()->check_out . ' hari ini ( ' . getToday() . ')',
                ], 200);
            } else {
                $absensi = $cekAbsen->update([
                    'check_out' => getHour(),
                    'check_out_long_lat' => $request->lat . ' , ' . $request->long,
                ]);

                return response()->json([
                    'message' => 'absen masuk berhasil terekam untuk ' . $hariIni,
                    'absensi' => $cekAbsen->first(),
                ], 200);
            }
        }
    }

    public function absensiToday()
    {
        $hariIni = getToday();
        $absensi = Absensi::where('tgl_absen', $hariIni)->where('user_id', auth()->user()->id)->first();
        return response()->json($absensi);
    }

    public function logAbsensi()
    {
        return Inertia::render('LogAbsensi', [
            'user' => auth()->user(),
            'absensi' => Absensi::with('user')->where('user_id', auth()->user()->id)->get()
        ]);
    }

    public function reportAbsensi()
    {

        return Inertia::render('ReportAbsensi', [
            'user' => auth()->user(),
            'year' => 2023,
            'month' => 5,
            'pegawai' => User::with('absensi')->where('role', 'pegawai')->get(),
            'baseSalary' => 1600000,
            'salaryPerCut' => 5000,
        ]);
    }
}
