<?php

namespace App\Http\Controllers;

use App\Models\Pembayaran;
use Illuminate\Http\Request;

class PembayaranController extends Controller
{
    public function store(Request $request)
    {


        $pembayaran = Pembayaran::create([
            'pesanan_id' => $request->id_pesanan,
            'metode_pembayaran' => 'Pembayaran dimuka',
            'tgl_jatuh_tempo' => null,
            'jenis_pembayaran' => $request->jenis_pembayaran,
            'jumlah_yang_dibayar' => $request->jumlah_yang_dibayar,
        ])->id;

        return response()->json([
            'message' => 'pesanan berhasil dibuat',
        ]);
    }
}
