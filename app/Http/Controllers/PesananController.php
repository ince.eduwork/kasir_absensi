<?php

namespace App\Http\Controllers;

use App\Models\Pelanggan;
use App\Models\Pembayaran;
use App\Models\Pesanan;
use App\Models\Produk;
use Illuminate\Http\Request;
use Inertia\Inertia;

class PesananController extends Controller
{

    public function getPesanan($idPesanan)
    {

        // Retrieve the appropriate 'soal' data based on the 'id_tryout'
        $soal = Pesanan::with('pelanggan')->with('produk')->with('pembayaran.pesanan.user')->with('user')->where('id', $idPesanan)->first();

        // Return the 'soal' data as a JSON response
        return response()->json($soal);
    }

    public function index($tgl_awal = null, $tgl_akhir = null)
    {

        if (!$tgl_awal || !$tgl_akhir) {
            $pesanan = Pesanan::with('pembayaran')->with('pelanggan')->with('produk')->get();
        } else {
            $pesanan = Pesanan::with('pembayaran')->with('pelanggan')->with('produk')->whereBetween('created_at', [$tgl_awal, $tgl_akhir])->get();
        }
        // dd(Pesanan::with('pelanggan')->with('produk')->get());
        return Inertia::render('Pesanan/Pesanan', [
            'user' => auth()->user(),
            'pesanan' => $pesanan,
        ]);
    }

    public function show($idPesanan)
    {
        return Inertia::render('Pesanan/Detail', [
            'user' => auth()->user(),
            'pesanan' => Pesanan::with('pelanggan')->with('produk')->with('pembayaran.pesanan.user')->with('user')->where('id', $idPesanan)->first(),
            'id_pesanan' => $idPesanan,
        ]);
    }

    public function print($idPesanan)
    {
        return Inertia::render('Pesanan/Print', [
            'user' => auth()->user(),
            'pesanan' => Pesanan::with('pelanggan')->with('produk')->with('pembayaran.pesanan.user')->with('user')->where('id', $idPesanan)->first(),
        ]);
    }

    public function create()
    {
        return Inertia::render('Pesanan/Create', [
            'user' => auth()->user(),
        ]);
    }

    public function store(Request $request)
    {

        $pelanggan = Pelanggan::create([
            'nama' => $request->nama_pelanggan,
            'email' => $request->email,
            'instansi' => $request->instansi,
            'telepon' => $request->telepon,
            'alamat' => $request->alamat,
            'jenis_kelamin' => $request->jenis_kelamin,
            'jenis_pelanggan' => $request->jenis_pelanggan,
        ])->id;

        $pesananId = Pesanan::create([
            'user_id' => auth()->user()->id,
            'pelanggan_id' => $pelanggan,
            // 'pembayaran_id' => $pembayaran,
            'deadline' => $request->deadline,
        ])->id;

        $pembayaran = Pembayaran::create([
            'pesanan_id' => $pesananId,
            'metode_pembayaran' => $request->metode_pembayaran,
            'tgl_jatuh_tempo' => $request->tgl_jatuh_tempo,
            'jenis_pembayaran' => $request->jenis_pembayaran,
            'jumlah_yang_dibayar' => $request->jumlah_yang_dibayar,
        ])->id;

        foreach ($request->pesanan as $pesanan) {
            Produk::create([
                'pesanan_id' =>  $pesananId,
                'nama' =>  $pesanan['nama'],
                'deskripsi' =>  $pesanan['deskripsi'],
                'bahan' =>  implode(',', $pesanan['bahan']),
                'mesin' =>  $pesanan['mesin'],
                'finishing' =>  $pesanan['finishing'],
                'jumlah' =>  $pesanan['jumlah'],
                'satuan' =>  $pesanan['satuan'],
                'satuan' =>  $pesanan['satuan'],
                'harga_satuan' =>  $pesanan['harga_satuan'],
                'total' => $pesanan['jumlah'] * $pesanan['harga_satuan']
            ]);
        }

        return response()->json([
            'message' => 'pesanan berhasil dibuat',
        ]);
    }
}
