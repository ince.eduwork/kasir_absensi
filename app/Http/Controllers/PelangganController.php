<?php

namespace App\Http\Controllers;

use App\Models\Pelanggan;
use Illuminate\Http\Request;
use Inertia\Inertia;

class PelangganController extends Controller
{
    public function index()
    {
        return Inertia::render('Pelanggan', [
            'user' => auth()->user(),
            'pelanggan' => Pelanggan::all(),
        ]);
    }
}
