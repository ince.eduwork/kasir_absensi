<?php

use App\Http\Controllers\AbsensiController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\GeneralController;
use App\Http\Controllers\PelangganController;
use App\Http\Controllers\PembayaranController;
use App\Http\Controllers\PesananController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\UserController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

// GENERAL CONTROLLER ROUTE
Route::group(['middleware' => ['auth', 'verified']], function () {
    Route::get('/dashboard', [DashboardController::class, 'index']);
    Route::get('/dashboard/{tgl_awal}/{tgl_akhir}', [DashboardController::class, 'index']);
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::get('/bantuan', [GeneralController::class, 'index']);
    Route::get('/absensi', [AbsensiController::class, 'absensi']);
    Route::get('/absensi/today', [AbsensiController::class, 'absensiToday']);
    Route::post('/absensi/check_in', [AbsensiController::class, 'check_in']);
    Route::post('/absensi/check_out', [AbsensiController::class, 'check_out']);
    Route::get('/log_absensi', [AbsensiController::class, 'logAbsensi']);
    Route::get('/pelanggan', [PelangganController::class, 'index']);
    Route::get('/pesanan', [PesananController::class, 'index']);
    Route::get('/pesanan/{tgl_awal}/{tgl_akhir}', [PesananController::class, 'index']);
    Route::get('/pesanan/create', [PesananController::class, 'create']);
    Route::get('/pesanan/print/{id_pesanan}', [PesananController::class, 'print']);
    Route::get('/pesanan/{id_pesanan}', [PesananController::class, 'show']);
    Route::post('/pesanan', [PesananController::class, 'store']);
    Route::get('/log_aktivitas', [GeneralController::class, 'logAktivitas']);
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});


// ADMIN ROUTE
Route::group(['middleware' => ['auth', 'ceklevel:admin,pegawai']], function () {
    Route::group(['prefix' => 'admin'], function () {
        // GET REQUEST
        Route::get('/pengguna', [UserController::class, 'pengguna']);

        // CRUD PENGGUNA
        Route::post('/pengguna', [UserController::class, 'store'])->name('pengguna.store');
        Route::put('/pengguna', [UserController::class, 'update'])->name('pengguna.update');
        Route::delete('/pengguna/{id}', [UserController::class, 'delete']);

        Route::get('/report_absensi', [AbsensiController::class, 'reportAbsensi']);

        Route::post('/pembayaran', [PembayaranController::class, 'store'])->name('pembayaran.store');
    });
});





require __DIR__ . '/auth.php';
